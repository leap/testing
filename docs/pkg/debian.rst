.. _debian:

Debian
======

This section documents all related to the debian package.


Dependencies
------------

* ``openvpn``
* ``python-qt4``
* ``python-crypto``
* ``python setuptools``
* ``python-requests``
* ``python-gnutls``

.. note::
   these two need a version that is not found in the current debian stable or in ubuntu 12.04. 
   They will be packaged... soon.

* ``python-gnutls == 1.1.9``
* ``python-keyring``

For tests
^^^^^^^^^
* ``python-nose``, ``python-mock``, ``python-coverage``

